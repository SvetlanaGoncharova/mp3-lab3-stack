# Методы программирования 2: Стек

[![Build Status](https://travis-ci.org/UNN-VMK-Software/mp2-lab1-set.svg)][travis]

<!-- TODO
  -
-->

## Цели и задачи

В рамках лабораторной работы ставится задача разработки двух видов стеков:

- прстейшего, основанного на статическом массиве (класс `TSimpleStack`);
- более сложного, основанного на использовании динамической структуры (класс `TStack`).

В процессе выполнения лабораторной работы требуется использовать систему контроля версий [Git][git] и фрэймворк для разработки автоматических тестов [Google Test][gtest].

Перед выполнением работы студенты получают данный проект-шаблон, содержащий следующее:

 - Интерфейсы классов `TDataCom` и `TDataRoot` (h-файлы)
 - Тестовый пример использования класса `TStack`

Выполнение работы предполагает решение следующих задач:

  1. Разработка класса `TSimpleStack` на основе массива фиксированной длины.
  1. Реализация методов класса `TDataRoot` согласно заданному интерфейсу.
  1. Разработка класса `TStack`, являющегося производным классом от `TDataRoot`.
  1. Разработка тестов для проверки работоспособности стеков.
  1. Обеспечение работоспособности тестов и примера использования.

## Используемые инструменты

  - Система контроля версий [Git][git]. Рекомендуется использовать один из
    следующих клиентов на выбор студента:
    - [Git](https://git-scm.com/downloads)
    - [GitHub Desktop](https://desktop.github.com)
  - Фреймворк для написания автоматических тестов [Google Test][gtest]. Не
    требует установки, идет вместе с проектом-шаблоном.
  - Среда разработки Microsoft Visual Studio (2008 или старше).
  - Опционально. Утилита [CMake](http://www.cmake.org) для генерации проектов по
    сборке исходных кодов. Может быть использована для генерации решения для
    среды разработки, отличной от Microsoft Visual Studio 2008 или 2010, предоставленных в данном проекте-шаблоне.

## Общая структура проекта

Структура проекта:

  - `gtest` — библиотека Google Test.
  - `include` — директория для размещения заголовочных файлов.
  - `samples` — директория для размещения тестового приложения.
  - `sln` — директория с файлами решений и проектов 
  - `src` — директория для размещения исходных кодов (cpp-файлы).
  - `test` — директория с модульными тестами и основным приложением,
    инициализирующим запуск тестов.
  - `README.md` — информация о проекте, которую вы сейчас читаете.


<!-- LINKS -->

[git]:         https://git-scm.com/book/ru/v2
[gtest]:       https://github.com/google/googletest


